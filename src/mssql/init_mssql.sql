Use master ;
GO
-- Set up Database
CREATE Database ManuWorks2021;
GO

Use ManuWorks2021;
GO

-- set up Schema
CREATE SCHEMA Sales;
GO
CREATE SCHEMA Manufacturing;
Go
-- Create Table

CREATE TABLE Manufacturing.Manufacturer (
    ManufacturerId VARCHAR(30) PRIMARY KEY,
    Name VARCHAR(50),
    NumOfWorkers INT,
    Location VARCHAR(50),
    MaxProductionCapacity INT NOT NULL
)
CREATE TABLE Manufacturing.Product(
    ProductId INT PRIMARY KEY,
    ProductName VARCHAR(50),
    ManufacturerId VARCHAR(30) NULL REFERENCES Manufacturing.Manufacturer(ManufacturerId),
    Color VARCHAR(50),
    Price DECIMAL(10,2) DEFAULT 0.00,
    Weight DECIMAL(8,2) DEFAULT 0.00
)

CREATE TABLE Manufacturing.Inventory(
    ProductId INT NOT NULL REFERENCES Manufacturing.Product(ProductId),
    InventoryDate DATE NOT NULL,
    TotalQty INT NOT NULL,
    QCPassQty INT NOT NULL
)
GO

ALTER TABLE Manufacturing.Inventory
ADD CONSTRAINT PK_Inventory PRIMARY KEY (ProductId, InventoryDate);
GO

CREATE TABLE Sales.Sales (
    SaleID INT PRIMARY KEY,
    OrderId VARCHAR(50),
    ProductId INT NOT NULL REFERENCES Manufacturing.Product(ProductId),
    Quantity INT NOT NULL,
    PercentDiscount DECIMAL(4,2),
    SaleDate DATE
)
-- Create Agent Job / Schedule

-- Create Stored Procedure
