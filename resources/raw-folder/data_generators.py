import pandas as pd
import csv
import random
from tqdm import tqdm
from datetime import datetime
from decimal import Decimal
from faker import Faker


fake = Faker()

def product(num_rows, mftid):
    proid = []; name = []; price = []; typ = []; weight = []; color = []; mft = []
    for i in tqdm(range(num_rows), desc= 'Creating product table'):
        proid.append(fake.unique.random_int(2123,2222+num_rows))
        name.append('PD' + fake.random_uppercase_letter() + str(fake.unique.random_int(num_rows,num_rows+num_rows)))
        weight.append(fake.random_int(1,5))
        price.append(fake.random_int(123456,328925))
        color.append(fake.color_name())
        mft.append(fake.random_element(elements=mftid))

    product = pd.DataFrame({'P_id': proid, 'P_name': name, 'P_manufactureid': mft, 'P_color': color, 
                            'P_price': price, 'P_weight': weight})

    return proid, product

def manufacture(num_rows):
    mftid = []; name = []; capa = []; locate = []; numworker = []
    for i in tqdm(range(num_rows), desc= 'Creating manufacture table'):
        name.append(fake.company() + ' ' + str(fake.unique.random_int(11,num_rows+11)))
        mftid.append('MF' + str(fake.unique.random_int(num_rows,num_rows+num_rows)))
        numworker.append(fake.random_int(1000,4000))
#         capa.append(fake.random_int(4234,8689))
        locate.append(fake.city())

    manufactory = pd.DataFrame({'M_id': mftid, 'M_name': name, 'M_worker':numworker,
                            'M_location': locate})

    return mftid, manufactory

def sale(num_rows, proid):
	odid = []; s_proid = []; qty = []; date = []; litemid = []; dcount = []; price = []
	for i in tqdm(range(num_rows), desc= 'Creating sale table'):
		litemid.append(i)
		s_proid.append(fake.random_element(elements=proid))
		odid.append('OD' + str(fake.random_int(0,56892)))
		qty.append(fake.random_int(1,30))
		date.append(fake.date_between(start_date='-5y', end_date='today'))
		dcount.append(fake.random_int(0,30))
		price.append(fake.random_int(454321,812345)*qty[i]*dcount[i]/100)

	sale = pd.DataFrame({'S_lineitem': litemid, 'S_orderid': odid, 'S_productid': s_proid, 'S_quantity': qty,
							'S_discount': dcount,'S_datetime': date})
	return sale

def inventory(num_rows, proid):
    i_proid = []; date = []; qcpass = []; qty = []; test = []
    for i in tqdm(range(num_rows), desc= 'Creating inventory table'):
        i_proid.append(fake.random_element(elements=proid))
        date.append(fake.date_between(start_date='-5y', end_date='today'))
        qty.append(fake.random_int(10,50))
        qcpass.append(round(qty[i]*fake.random_int(85,100)/100))

    inventory = pd.DataFrame({'I_proid': i_proid, 'I_date': date, 'I_quantity': qty, 'I_qcpass': qcpass})

    return inventory

if __name__ == '__main__':
	mftid, manufactory = manufacture(20)
	proid, product = product(300, mftid)
	inventory = inventory(700000, proid)
	product.to_csv('Product.csv', index=False)
	inventory.to_csv('Inventory.csv', index = False)
	max_inven_qty = inventory.groupby('I_proid').max('I_quantity')
	product = product.merge(max_inven_qty['I_quantity'], left_on='P_id', right_on='I_proid')
	capacity = product.groupby('P_manufactureid').sum()
	manufactory = manufactory.merge(capacity['I_quantity'], left_on = 'M_id', right_on = 'P_manufactureid')
	manufactory['I_quantity'] =  manufactory['I_quantity'] + fake.random_int(100,1000)
	manufactory = manufactory.rename(columns={'I_quantity':'M_capacity'})
	manufactory.to_csv('Manufactory.csv',index=False)
	sale = sale(1000000, proid)
	sale.to_csv('Sale.csv', index=False)
# 	sale = sale(100000, proid)
# 	manufactory.to_csv('Manufactory.csv',index=False)
# 	product.to_csv('Product.csv', index=False)
# 	sale.to_csv('Sale.csv', index=False)
#     print('Creating a fake data...')
#     create_csv_file_Order_Line()
