# Decsription of this project
## Purpose

Building a data pipeline from start to Power BI.

This project is used to demo our current growing knowledge of data pipelines using SSIS. The topic is "Manufacturing - Material & stock prediction based on historical consumption information and warehouse/production line capacity"

This go from Data generation, to staging, to snowflake warehouse

The Power BI Dashboard can be found through [this link](https://app.powerbi.com/links/3vUoOZOaBm?ctid=f01e930a-b52e-42b1-b70f-a8882b5d043b&pbi_source=linkShare) (until the end of November). Only members of the organization can view it.

Otherwise, the Power BI source file is in [./src/powerbi](./src/powerbi)

## Pre-requisite
**\*Please Note\*** that this project uses several technologies. Please install and equip them:
- Python 3 (and several modules. We have provided [py_requirements.txt] file for pip-installing these modules. You could also [use Conda](https://docs.conda.io/projects/conda/en/4.6.1/user-guide/getting-started.html) for convenience).
- The latest version of [ODBC Data Sources (32-bit) for Snowflake](https://sfc-repo.snowflakecomputing.com/odbc/win32/index.html)
- Windows 10
- SQL Server - Use SSMS
- Visual Studio
- SQL Server Integration Services (SSIS). [Link here](https://docs.microsoft.com/en-us/sql/integration-services/sql-server-integration-services?view=sql-server-ver15).
- Power BI

## Structures

Staging tables
![Staging Table](./docs/Stage_DB_Diagram.png)

Dim and Fact Tables
![Fact and Dim Tables Schema](./docs/dw_diagram.png)

Project Folder Structure
![Project Folder Structure](./docs/Project_folder.png)

## DISCLAIMER
All data generated, all code written, and all files are solely created by the group members who own this git repository. This means that this entire gitlab repository is essentially non-affiliated with 'the company' or any of its clients.


# Detail of Work

1. Design data pipeline [here](./docs/design.png "Architecture")
2. Normalize and Denormalize data
3. Build data model [here](./docs/Stage_DB_Diagram.png "Stage Database Diagram")
4. Ingest data from flat file
5. Extract and Load into Data warehouse 
6. Load data onto Cloud with the transformation
7. Enrich data with different data sources
8. Visualize your data

# SSIS Solution Structure
There are 2 packages:
1. *To Staging*
2. *Staging To Snowflake*
Please open the .sln from [src/Manufacturing_Reports](./src/Manufacturing_Reports)

# How to setup: Basic
1. Login into MSSQL and run [init_mssql.sql](./src/mssql/init_mssql.sql)
2. Authen SnowSQL and run [init_snowflake.sql](./src/mssql/init_snowfalke.sql)
3. Generate data: `python data-generators.py`
4. Install and configure ODBC Data Sources (32-bit) connections. We will create 2 System .thingy........................ for each Schemas
    - snow_dw_Manu: for Manufacturing Schema
    - snow_dw_Sales: for Sales Schema

# How to Setup 2: SSIS
We have configured SSIS OLE DB connection manager to use "localhost" as the local database server name, for dynamic pathing. So please check several things before moving on.

1. Dynamic Pathing
 - Copy your "Team9_Project01" Project file path. 
    - E.g. C:\Path\path\Team9_Project01
    - [Support Document](https://www.sony.com/electronics/support/articles/00015251)
 - For each package, please access the Variables and look for ones called "ProjectFolderAbsolutePath" or "ProjectAbsolutePath"
 - Edit "Value"
    - Paste the copied Project file path
    - Add a backslash at the end (\\)
    - E.g. C:\Path\path\Team9_Project01\
2. Making sure the local db server is up and running, and can be called as "localhost"
3. ODBC (32-bit) Configuration - Name them snow_dw_Manu and snow_dw_Sales (with your own Snowflake datawarehouse information like **server name,schema, warehouse**)
4. Turn off SSIS 64bit Configuration
5. Extra: if your naming is different for each ODBC connectors to each Snowflake schemas, then go to the Connection Manager section and re-configure the ODBC connection managers with names "MANU_MN" and "MANU_SA"

